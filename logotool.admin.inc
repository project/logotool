<?php

/**
 * @file
 * Logotool admin settings file.
 */

/**
 * Menu callback: Settings.
 */
function logotool_settings() {
  $folder_path =  variable_get('file_default_scheme', 'public') . '://' . variable_get('logotool_folder', LOGOTOOL_CWD);
  if (!file_prepare_directory($folder_path, FILE_CREATE_DIRECTORY)) {
    form_set_error('logotool_folder', t('The directory %directory does not exist or is not writable.', array('%directory' => variable_get('logotool_folder', LOGOTOOL_CWD))));
    watchdog('file system', 'The directory %directory does not exist or is not writable.', array('%directory' => variable_get('logotool_folder', LOGOTOOL_CWD)), WATCHDOG_ERROR);
  }
  $form['general']['logotool_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to default/ main logo'),
    '#description' => t('The path (relative to this Drupal installation) to the file you would like to use as your default/ main logo (NO leading slash).'),
    '#default_value' => variable_get('logotool_path', 'misc/druplicon.png'),
  );
  $form['general']['logotool_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to your logos folder'),
    '#description' => t('Subdirectory in the directory %dir where logos will be uploaded.', array('%dir' => file_directory_path() . '/')),
    '#default_value' => variable_get('logotool_folder', LOGOTOOL_CWD),
  );
  $form['general']['logotool_display'] = array(
    '#type' => 'select',
    '#title' => t('Display mode'),
    '#description' => t('<p><strong>Normal mode</strong>: Displays the default/ main logo on every page.</p>
      <p><strong>Random logos</strong>: Displays a random logo on every page refresh.</p>
      <p><strong>Specified logos</strong>: Displays a specific logo on each specified page from the
      information below, pages not specified will display the default/ main logo.</p>
      <p><strong>Specified AND Random logos</strong>: Displays a specific logo on each specified page from the
      information below, pages not specified will display a random logo.</p>'),
    '#options' => _logotool_which_mode(),
    '#default_value' => variable_get('logotool_display', LOGOTOOL_NORMAL),
  );
  $form['general']['logotool_frequency'] = array(
    '#type' => 'select',
    '#title' => t('Random Frequency'),
    '#description' => t("Select how often your logo is rotated (if you've set it to be random)."),
    '#options' => array(0 => t('Every page refresh'), 60 => t('Every minute'), 3600 => t('Every hour'), 86400 => t('Daily')),
    '#default_value' => variable_get('logotool_frequency', 0),
  );

  $form['templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Templating'),
    '#description' => t('For extra functionality, e.g. this could be used to set a title/ alt tags for your logos...<br /><strong>Requires a basic level of css and template knowledge.</strong>
      <br />Be aware this is powerful functionality and with it you can cause much mischief and havoc, e.g. the per-page flexibility of manipulating page variables allows you to even set an individual page title for every page on your site.'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['templates']['logotool_use_templating'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use templating functionality'),
    '#description' => t('<strong>Caution</strong>: This is only available on this administrative page as no unmanaged user should be able to manipulate template variables without supervision.<br />If you do not understand how this functionality works, I highly suggest you <strong>DO NOT USE IT!</strong>'),
    '#default_value' => variable_get('logotool_use_templating', FALSE),
  );
  $form['templates']['logotool_template'] = array(
    '#type' => 'textarea',
    '#title' => t('Set template variables for specific pages'),
    '#description' => t('(Optional) Set template variables here, for a very basic usage example;
      <p>In your <strong>page.tpl.php</strong> file:
      <br /><code>&lt;?php print \'&lt;img src="\'. check_url($logo) .\'" alt="\'. $logotool_alt .\'" id="logo" /&gt;\'; ?&gt;</code>
      <br /><code>&lt;div class="&lt;?php print $logotool_colour; ?>"&gt;*something*&lt;/div&gt;</code>
      <p>And your <strong>style.css</strong>:
      <br /><strong>div.blue{ color:blue; }<br/>div.red{ color:red; }</strong></p>
      <p>Logotool template entries:
      <br /><strong>&lt;front&gt;|logotool_alt|Testing<br />&lt;front&gt;|logotool_colour|blue<br />node/1|logotool_colour|red</strong></p>'),
    '#default_value' => variable_get('logotool_template', ''),
    '#cols' => 35,
    '#rows' => 20,
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logo Page/ Upload Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );
  if (module_exists('image')) {
    $form['settings']['logotool_image_style'] = array(
      '#type' => 'select',
      '#title' => t('image style'),
      '#description' => t('If desired, select an image style for use <strong>solely</strong> on the <a href="!logo-tab-link">logo management</a> page. Using one may make the display more manageable depending on the size of your logos.<br />Please be aware that with any image manipulation such as this, animated gifs will not survive this process...', array('!logo-tab-link' => url('admin/settings/logotool/logos'))),
      '#options' => _logotool_image_styles(),
      '#default_value' => variable_get('logotool_image_style', ''),
    );
  }
  $form['settings']['logotool_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed upload extensions'),
    '#description' => t('Extensions that can be uploaded. Separate extensions with a space and do not include the leading dot.'),
    '#required' => TRUE,
    '#default_value' => variable_get('logotool_extensions', 'jpg jpeg gif png'),
  );
  $form['settings']['logotool_max_resolution'] = array('#type' => 'textfield',
    '#title' => t('Maximum resolution for uploaded images'),
    '#default_value' => variable_get('logotool_max_resolution', 0),
    '#size' => 15,
    '#maxlength' => 10,
    '#description' => t('The maximum allowed image size for upload (e.g. 640x480). Set to 0 for no restriction. If an <a href="!image-toolkit-link">image toolkit</a> is installed, files exceeding this value will be scaled down to fit.', array('!image-toolkit-link' => url('admin/settings/image-toolkit'))),
    '#field_suffix' => '<kbd>' . t('WIDTHxHEIGHT') . '</kbd>',
  );
  $form['settings']['logotool_upload_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum file size per upload'),
    '#description' => t('The maximum file size that can be uploaded. If an image is uploaded and a maximum resolution is set, the size will be checked after the file has been resized.'),
    '#default_value' => variable_get('logotool_upload_size', 1),
    '#field_suffix' => t('MB'),
  );
  $form['settings']['upload_max_size'] = array('#value' => '<p>' . t('Your PHP settings limit the maximum file size per upload to %size.', array('%size' => format_size(file_upload_max_size()))) . '</p>');

  return system_settings_form($form);
} // logotool_settings

/**
 * Validation for settings form.
 */
function logotool_settings_validate(&$form, &$form_state) {
  if ($form_state['values']['logotool_upload_size'] > format_size(file_upload_max_size())) {
    form_set_error('logotool_upload_size', t('Upload size exceeds the limit allowed by your php installation. Please contact your system administrator.'));
  }
} // logotool_settings_validate