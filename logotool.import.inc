<?php

/**
 * @file
 * Logotool import from directory functionality.
 */

/**
 * Form to collect system path to import directory.
 */
function logotool_import_form() {
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory path'),
    '#description' => t('The path (relative to this Drupal installation) to the folder you wish to import from.'),
  );
  $form['commit'] = array(
    '#type' => 'submit',
    '#value' => t('Action!'),
  );
  return $form;
} // logotool_import_form

/**
 * Validation callback for import form.
 */
function logotool_import_form_validate(&$form, &$form_state) {
  if (!file_prepare_directory($form_state['values']['path'])) {
    form_set_error('path', t("Path '@path' does not exist.", array('@path' => $form_state['values']['path'])));
  }
} // logotool_import_form_validate

/**
 * Submit callback for import form.
 */
function logotool_import_form_submit($form, &$form_state) {
  if ($logos = $form_state['values']['path']) {
    global $user;
    $files = file_scan_directory($logos, '/.*/');
    foreach ((array)$files as $path => $file) {
      // Copy file and save to files table or report that it already exists.
      if ((bool) db_query_range("SELECT 1 FROM {logotool} l INNER JOIN {file} f ON f.fid = l.fid WHERE f.filename = :filename", 0, 1, array(':filename' => $file->filename))->fetchField()) {
        drupal_set_message(t("Logo file '@filename' already exists, skipping...", array('@filename' => $file->filename)), 'error');
      }
      else {
        $file->uri = file_unmanaged_copy($path, variable_get('file_default_scheme', 'public') . '://' . variable_get('logotool_folder', LOGOTOOL_CWD));
        $file->filemime = file_get_mimetype($path);
        $file->uid = $user->uid;
        $file->status |= FILE_STATUS_PERMANENT;
        file_save($file);
        db_insert('logotool')->fields(array('fid' => $file->fid, 'pages' => ''))->execute();
        drupal_set_message(t("Imported logo file '@filename'.", array('@filename' => $file->filename)));
      }
    }
  }
} // logotool_import_form_submit